import { Movie } from './movie.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Injectable()
export class MovieService {
  movieChange = new Subject<Movie[]>();
  movieFetching = new Subject<boolean>();
  addMovieSpinner = new Subject<boolean>()
  movies: Movie[] = [];

  constructor(private http: HttpClient) {
  }

  getMoviesInServer() {
    this.movieFetching.next(true);
    this.http.get<{ [id: string]: Movie }>('https://projectsattractor-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(key => {
          const keyMovie = result[key];
          return new Movie(keyMovie.title, key);
        })
      }))
      .subscribe(movies => {
        this.movies = movies;
        this.movieChange.next(this.movies.slice());
        this.movieFetching.next(false);
      });
  }

  getMovies() {
    return this.movies.slice();
  }

  sendMovie(movie: string) {
    const body = {
      title: movie,
    }
    this.addMovieSpinner.next(true);
    this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/movies.json', body).subscribe(() => {
      this.getMoviesInServer();
      this.addMovieSpinner.next(false);
    });
  }

  deleteMovie(id: string) {
    this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com//movies/${id}.json`).subscribe(() => {
      this.getMoviesInServer();
    });
  }
}
