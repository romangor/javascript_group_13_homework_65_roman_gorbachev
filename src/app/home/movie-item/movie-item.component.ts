import { Component, Input, OnInit } from '@angular/core';
import { MovieService } from 'src/app/shared/movie.service';
import { Movie } from '../../shared/movie.model';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.css']
})
export class MovieItemComponent implements OnInit {
  @Input() movie!: Movie;
  loading: boolean = false;
  movies: Movie[] = [];

  constructor(private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
  }

  deleteMovie(id: string) {
    this.movieService.deleteMovie(id);
    this.loading = true;
  }
}
