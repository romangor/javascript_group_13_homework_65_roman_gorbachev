import { Component, OnInit } from '@angular/core';
import { Movie } from '../shared/movie.model';
import { MovieService } from '../shared/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  movies: Movie[] = [];
  titleMovie!: string;
  loading: boolean = false;
  loadingButton: boolean = false;

  constructor(private movieService: MovieService) {
  }

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
    this.movieService.movieChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    });
    this.movieService.movieFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    });
    this.movieService.getMoviesInServer();
  }

  addMovie() {
    const movie = this.titleMovie;
    this.movieService.sendMovie(movie);
    this.movieService.addMovieSpinner.subscribe((isAdding: boolean) => {
      this.loadingButton = isAdding;
      console.log(isAdding);
    });
  }

}
